﻿using Hiplatform.SkyGenerator.Importer;
using Hiplatform.SkyGenerator.Importer.Models;
using Hiplatform.SkyGenerator.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Hiplatform.SkyGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
           .SetBasePath(Directory.GetCurrentDirectory())
           .AddJsonFile("appsettings.json");

            var configuration = builder.Build();

            Console.WriteLine("Starting upload");
            string[] records = File.ReadAllLines(configuration["CSVPath"]);
            var _importer = new CSVImporter<SkyCSVModel>();

            List<SkyCSVModel> listResult = _importer.Import(records).ToList();
            List<string> errors = SkyCSVModel.ValidateModel(listResult);
            if (errors.Count() > 0)
                errors.ForEach(e => Console.WriteLine(e));
            else
                HtmlGenerator.Generate(SkyElement.Convert(listResult), SkyElement.GetTiers(listResult), configuration);

            Console.WriteLine("Done.");
            Console.WriteLine("\n\nPress enter to end...");
            Console.Read();
        }

        
    }
}
