﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hiplatform.SkyGenerator.Models
{
    interface ISelector : IElement
    {
        Dictionary<string, IElement> Options { get; set; }
    }
}
