﻿using Hiplatform.SkyGenerator.Importer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hiplatform.SkyGenerator.Models
{
    public class SkyElement : IElement
    {
        public SkyElement(string tier, string idd, string redirect, string origin)
        {
            Tier = tier;
            Idd = idd;
            Redirect = redirect;
            Origin = origin;
        }
        public string Tier { get; set; }
        public string Idd { get; set; }
        public string Redirect { get; set; }
        public string Origin { get; set; }

        public override string ToString()
        {
            return Tier + "-" + Idd + "-" + Redirect;
        }

        public static Dictionary<string, IElement> Convert(IEnumerable<SkyCSVModel> list)
        {
            var root = new Dictionary<string, IElement>();
            foreach (var model in list)
            {
                var levelOne = SkyElement.GetSelector(root, model.Level1) as Selector;
                var levelTwo = SkyElement.GetSelector(levelOne.Options, model.Level2) as Selector;
                var levelThree = SkyElement.GetSelector(levelTwo.Options, model.Level3) as Selector;
                var skyValue = new SkyElement(model.Tier, model.IDD, model.Redirect, model.Origin);

                if (levelThree == null)
                {
                    if (!levelTwo.Options.ContainsKey(skyValue.Tier))
                        levelTwo.Options.Add(skyValue.Tier, skyValue);
                    else
                        throw new Exception("Existe 2 fluxos iguais " + model.Level1 + "-" + model.Level2 + "-" + model.Level3 + "-" + skyValue.ToString());
                }
                else
                {
                    if (!levelThree.Options.ContainsKey(skyValue.Tier))
                        levelThree.Options.Add(skyValue.Tier, skyValue);
                    else
                        throw new Exception("Existe 2 fluxos iguais " + model.Level1 + "-" + model.Level2 + "-" + model.Level3 + "-" + skyValue.ToString());
                }
            }
            return root;
        }

        public static IElement GetSelector(Dictionary<string, IElement> dic, string data)
        {
            IElement selector = null;
            if (dic.ContainsKey(data))
            {
                selector = dic[data];
            }
            else if (!string.IsNullOrEmpty(data) && data.Trim() != "-")
            {
                selector = new Selector();
                dic.Add(data, selector);
            }
            return selector;
        }

        public static IEnumerable<string> GetTiers(IEnumerable<SkyCSVModel> list)
        {
            var result = new List<string>();
            foreach (var model in list)
            {
                if (!result.Contains(model.Tier))
                    result.Add(model.Tier);
            }
            return result;
        }
    }
}
