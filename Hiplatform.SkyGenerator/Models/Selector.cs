﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hiplatform.SkyGenerator.Models
{
    public class Selector : ISelector
    {
        public Selector()
        {
            Options = new Dictionary<string, IElement>();
        }
        public Dictionary<string, IElement> Options { get; set; }
    }
}
