﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hiplatform.SkyGenerator.Importer
{
    /// <summary>
    /// Atributo com o nome do csv
    /// </summary>
    public class HeaderAttribute : Attribute
    {
        /// <summary>
        /// Nome do campo no csv
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// É obrigatório ou não
        /// </summary>
        public bool Required { get; set; }

        /// <summary>
        /// Construtor com o tipo
        /// </summary>
        /// <param name="name"></param>
        public HeaderAttribute(string name)
        {
            this.Name = name;
        }
    }
}
