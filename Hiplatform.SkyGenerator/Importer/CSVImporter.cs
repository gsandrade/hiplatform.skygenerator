﻿using Hiplatform.SkyGenerator.Importer.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Hiplatform.SkyGenerator.Importer
{
    /// <summary>
    /// Importador de CSV
    /// </summary>
    /// <typeparam name="T">Modelo</typeparam>
    public class CSVImporter<T> : IImporter<T, string[]>
    {
        /// <summary>
        /// Caracter que separa uma coluna de outra
        /// </summary>
        const char DELIMITER = ';';

        /// <summary>
        /// Faz a validação se o header está com os campos obrigatórios
        /// </summary>
        /// <param name="arrayHeader"></param>
        private void HeaderRequiredValidator(string[] arrayHeader)
        {
            List<HeaderAttribute> requiredAttributes = new List<HeaderAttribute>();
            foreach (var item in typeof(T).GetProperties())
            {
                var objectCustomAttribute = item.GetCustomAttribute(typeof(HeaderAttribute));
                if (objectCustomAttribute != null && ((HeaderAttribute)objectCustomAttribute).Required)
                {
                    requiredAttributes.Add((HeaderAttribute)objectCustomAttribute);
                }
            }

            int headerJoinQuantify = arrayHeader.Join(requiredAttributes, x => x, y => y.Name.Trim(), (x, y) => new { Value = y }).Count();
            if (headerJoinQuantify != requiredAttributes.Count())
            {
                throw new RequiredHeaderException("Existem headers obrigatórios não preenchidos", requiredAttributes);
            }

        }

        /// <summary>
        /// Devolve dicionario com o indice da coluna e qual propriedade é do modelo
        /// </summary>
        /// <param name="arrayHeader"></param>
        /// <param name="properties"></param>
        /// <returns></returns>
        private Dictionary<int, PropertyInfo> GetAssemblyProp(string[] arrayHeader, PropertyInfo[] properties)
        {
            Dictionary<string, int> headerMap = new Dictionary<string, int>();
            for (int i = 0; i < arrayHeader.Length; i++)
            {
                headerMap.Add(arrayHeader[i], i);
            }

            Dictionary<int, PropertyInfo> propertiesMap = new Dictionary<int, PropertyInfo>();
            foreach (PropertyInfo propertyInfo in properties)
            {
                var objectCustomAttribute = propertyInfo.GetCustomAttribute(typeof(HeaderAttribute));
                if (objectCustomAttribute != null && headerMap.ContainsKey(((HeaderAttribute)objectCustomAttribute).Name))
                {
                    propertiesMap.Add(headerMap[((HeaderAttribute)objectCustomAttribute).Name], propertyInfo);
                }
            }

            return propertiesMap;

        }

        /// <summary>
        /// Transforma array de string em um modelo
        /// </summary>
        /// <param name="imported"></param>
        /// <returns></returns>
        public IEnumerable<T> Import(string[] imported)
        {
            string[] header = imported[0].Split(DELIMITER);
            HeaderRequiredValidator(header);

            var body = imported.Skip(1);

            PropertyInfo[] propertyInfos = typeof(T).GetProperties();
            var headerMap = GetAssemblyProp(header, propertyInfos);

            foreach (var line in body)
            {
                if (String.IsNullOrEmpty(line))
                    continue;
                else if (String.IsNullOrEmpty(line.Replace(";", string.Empty)))
                    continue;

                var fields = line.Split(DELIMITER);

                T instance = (T)Activator.CreateInstance(typeof(T));
                PropertyInfo[] propertiesOfInstance = instance.GetType().GetProperties();
                for (int i = 0; i < fields.Length; i++)
                {
                    if (headerMap.ContainsKey(i))
                    {
                        var propertyInfo = propertiesOfInstance.FirstOrDefault(x => x.Name == headerMap[i].Name);
                        if (propertyInfo != null)
                        {
                            propertyInfo.SetValue(instance, Convert.ChangeType(fields[i], propertyInfo.PropertyType), null);
                        }
                    }
                }

                yield return instance;
            }

        }
    }
}
