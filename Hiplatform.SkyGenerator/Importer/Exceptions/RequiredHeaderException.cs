﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hiplatform.SkyGenerator.Importer.Exceptions
{
    /// <summary>
    /// Exceção que diz que não possui os headers obrigatórios
    /// </summary>
    public class RequiredHeaderException : Exception
    {
        /// <summary>
        /// Headers obrigatórios
        /// </summary>
        public List<HeaderAttribute> RequiredHeaders { get; set; }

        /// <summary>
        /// Construção da exceção
        /// </summary>
        /// <param name="message">Mensagem de erro</param>
        /// <param name="requiredHeaders">Headers obrigatórios</param>
        public RequiredHeaderException(string message, List<HeaderAttribute> requiredHeaders)
            : base(message)
        {
            this.RequiredHeaders = requiredHeaders;
        }
    }
}
