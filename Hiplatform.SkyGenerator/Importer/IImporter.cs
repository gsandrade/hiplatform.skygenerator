﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hiplatform.SkyGenerator.Importer
{
    /// <summary>
    /// Interface de Importador
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="U"></typeparam>
    public interface IImporter<T, U>
    {
        /// <summary>
        /// Importar
        /// </summary>
        /// <param name="importData">dado a ser importado</param>
        /// <returns></returns>
        IEnumerable<T> Import(U importData);
    }
}
