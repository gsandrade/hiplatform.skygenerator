﻿
using Hiplatform.SkyGenerator.Importer.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Hiplatform.SkyGenerator.Importer.Models
{
    [Serializable]
    public class SkyCSVModel
    {
        [HeaderAttribute("Nível 1", Required = true)]
        [Required(ErrorMessage = "está com Nível 1 sem preenchimento", AllowEmptyStrings = false)]
        public string Level1 { get; set; }

        [HeaderAttribute("Nível 2", Required = true)]
        [Required(ErrorMessage = "está com Nível 2 sem preenchimento", AllowEmptyStrings = false)]
        public string Level2 { get; set; }

        [HeaderAttribute("Nível 3", Required = true)]
        [Required(ErrorMessage = "está com Nível 3 sem preenchimento", AllowEmptyStrings = true)]
        public string Level3 { get; set; }

        [HeaderAttribute("Redirecionamento", Required = true)]
        [Required(ErrorMessage = "está com Redirecionamento sem preenchimento", AllowEmptyStrings = false)]
        public string Redirect { get; set; }

        [HeaderAttribute("IDD", Required = true)]
        [Required(ErrorMessage = "está com IDD sem preenchimento", AllowEmptyStrings = false)]
        public string IDD { get; set; }

        [HeaderAttribute("Tier", Required = true)]
        [Required(ErrorMessage = "está com Tier sem preenchimento", AllowEmptyStrings = false)]
        public string Tier { get; set; }

        [HeaderAttribute("Origem", Required = true)]
        [Required(ErrorMessage = "está com Origem sem preenchimento", AllowEmptyStrings = false)]
        public string Origin { get; set; }

        public static List<string> ValidateModel(List<SkyCSVModel> models)
        {
            List<string> errors = new List<string>();
            foreach (var item in models)
            {
                var error = ValidationHelper.ValidateEntity<SkyCSVModel>(item);

                if (error.HasError)
                {
                    errors.Add(
                        "Nível 1: " + item.Level1 + 
                        "\nNível 2: " + item.Level2 +
                        "\nNível 3: " + item.Level3 +
                        "\nRedirect: " + item.Redirect +
                        "\nIDD: " + item.IDD +
                        "\nTier: " + item.Tier +
                        "\nOrigem: " + item.Origin +
                        "\nError: " + error.StringErrors());
                }
            }
            return errors;
        }
    }
}
