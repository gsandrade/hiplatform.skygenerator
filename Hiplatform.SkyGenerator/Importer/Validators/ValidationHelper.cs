﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace Hiplatform.SkyGenerator.Importer.Validators
{
    /// <summary>
    /// Resultado da validação
    /// </summary>
    public class EntityValidationResult
    {
        /// <summary>
        /// Lista com os erros
        /// </summary>
        public IList<ValidationResult> Errors { get; private set; }

        /// <summary>
        /// Entidade que diz se existe erro ou não
        /// </summary>
        public bool HasError
        {
            get { return Errors.Count > 0; }
        }

        /// <summary>
        /// Construtor com os erros
        /// </summary>
        /// <param name="errors"></param>
        public EntityValidationResult(IList<ValidationResult> errors = null)
        {
            Errors = errors ?? new List<ValidationResult>();
        }

        /// <summary>
        /// Transforma erros em uma string
        /// </summary>
        /// <returns></returns>
        public string StringErrors()
        {
            return this.Errors
                .Select(x => x.MemberNames.FirstOrDefault() + " " + x.ErrorMessage + "\n")
                .Aggregate((current, next) => current + Environment.NewLine + next);
        }
    }

    /// <summary>
    /// Validador da entidade
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EntityValidator<T> where T : class
    {
        /// <summary>
        /// Método responsável pela validação
        /// </summary>
        /// <param name="entity">Entidade</param>
        /// <returns></returns>
        public EntityValidationResult Validate(T entity)
        {
            var validationResults = new List<ValidationResult>();
            var vc = new ValidationContext(entity, null, null);
            var isValid = Validator.TryValidateObject(entity, vc, validationResults, true);

            return new EntityValidationResult(validationResults);
        }
    }

    /// <summary>
    /// Façade de Validador de entidade
    /// </summary>
    public class ValidationHelper
    {
        /// <summary>
        /// Método estático com a validação da entidade
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static EntityValidationResult ValidateEntity<T>(T entity)
            where T : class
        {
            return new EntityValidator<T>().Validate(entity);
        }
    }
}
