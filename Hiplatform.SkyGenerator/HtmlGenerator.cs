﻿using Hiplatform.SkyGenerator.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Hiplatform.SkyGenerator
{
    public class HtmlGenerator
    {
        public static void Generate(Dictionary<string, IElement> list, IEnumerable<string> tiers, IConfigurationRoot configuration)
        {
            using (FileStream fs = new FileStream(configuration["HTMLPath"], FileMode.Create))
            {
                using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                {
                    w.Write(Build(list, tiers, configuration));
                }
            }
        }

        public static string Build(Dictionary<string, IElement> list, IEnumerable<string> tiers, IConfigurationRoot configuration)
        {
            var test = true;
            var str = new StringBuilder();

            if(test)
                str.AppendLine(@"<!doctype html><html lang=""en"" ng-app=""app"" ><head><meta charset=""UTF-8"" ><link rel=""stylesheet"" href=""<%=css%>""><script src=""<%= js %>""></script><script>angular.module(""app"").constant(""CSRF_TOKEN"", 'foobar');</script></head><body><div class=""row"" ><div class=""large-12"">");

            str.AppendLine(@"<form novalidate="""" name=""form"" action=""https://www2.directtalk.com.br/chat31/?"" ng-submit=""submit(form, $event, false, formData.article)"">");

            #region [ Tier e Origem ]
            foreach(var tier in tiers)
            {
                str.AppendLine(string.Format(@"<input type=""text"" name=""{0}"" ng-model=""formData.{1}"" style=""display:none"" class=""hi-sky-tiers"" />", Utils.Slugfy(tier), Utils.Slugfy(tier)));
            }
            if(test)
                str.AppendLine(@"<input type=""text"" name=""origem"" ng-model=""formData.origem"" style=""display:none"" ng-init=""formData.origem='site_sky'"" /> ");
            else
                str.AppendLine(@"<input type=""text"" name=""origem"" ng-model=""formData.origem"" style=""display:none"" /> ");
            #endregion

            #region [ Artigos ]
            str.AppendLine(@"<input type=""text"" style=""display:none"" name=""artigosky"" ng-model=""formData.artigosky"" ng-if=""!(formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Financeiro' && formData.terceiroCombo === '2_via_de_fatura') &&!(formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Financeiro' && formData.terceiroCombo === 'Mudanca_de_Vencimento') &&!(formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Financeiro' && formData.terceiroCombo === 'Pagar_valores_em_aberto') &&!(formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Tecnico' && formData.terceiroCombo === 'Ausencia_de_sinal') &&!(formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Tecnico' && formData.terceiroCombo === 'Codigo_1_2_5_7_8_11_ou_25') &&!(formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Tecnico' && formData.terceiroCombo === 'Codigo_6') &&!(formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Tecnico' && formData.terceiroCombo === 'Codigo_13') &&!(formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Tecnico' && formData.terceiroCombo === 'Codigo_77') &&!(formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Tecnico' && formData.terceiroCombo === 'Codigo_109') &&!(formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Tecnico' && formData.terceiroCombo === 'Codigo_771_775_ou_776') &&!(formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Pacotes_e_Programacao' && formData.terceiroCombo === 'Big_Brother_Brasil') &&!(formData.origem && formData.primeiroCombo === 'sky_play') &&!(formData.origem && formData.primeiroCombo === 'sky_pre_pago' && formData.segundoCombo === 'Codigo_109') &&!(formData.origem && formData.primeiroCombo === 'sky_banda_larga' && formData.segundoCombo === 'Tecnico') &&!(formData.origem && formData.primeiroCombo === 'sky_banda_larga' && formData.segundoCombo === 'Cobranca') &&!(formData.origem && formData.primeiroCombo === 'sky_banda_larga' && formData.segundoCombo === 'Aquisicao') &&!(formData.origem && formData.primeiroCombo === 'sky_banda_larga' && formData.segundoCombo === 'Planos') &&!(formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Tecnico' && formData.terceiroCombo === 'Visita_Técnica') &&!(formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Financeiro' && formData.terceiroCombo === 'Contestacao_de_valores') &&!(formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Financeiro' && formData.terceiroCombo === 'Duvida_na_fatura')"" ng-init=""formData.artigosky = ''"">");
            str.AppendLine(@"<input type=""text"" style=""display:none"" name=""artigosky"" ng-model=""formData.artigosky"" ng-if=""formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Financeiro' && formData.terceiroCombo === '2_via_de_fatura'""  ng-init=""formData.artigosky = 'Solicitação da segunda via da fatura'"">");
            str.AppendLine(@"<input type=""text"" style=""display:none"" name=""artigosky"" ng-model=""formData.artigosky"" ng-if=""formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Financeiro' && formData.terceiroCombo === 'Mudanca_de_Vencimento'"" ng-init=""formData.artigosky = 'Deseja alterar o vencimento da fatura?'"">");
            str.AppendLine(@"<input type=""text"" style=""display:none"" name=""artigosky"" ng-model=""formData.artigosky"" ng-if=""formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Financeiro' && formData.terceiroCombo === 'Pagar_valores_em_aberto'""  ng-init=""formData.artigosky = 'Pagar um valor em aberto'"">");
            str.AppendLine(@"<input type=""text"" style=""display:none"" name=""artigosky"" ng-model=""formData.artigosky"" ng-if=""formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Tecnico' && formData.terceiroCombo === 'Ausencia_de_sinal'""  ng-init=""formData.artigosky = 'Instabilidade no sinal da SKY'"">");
            str.AppendLine(@"<input type=""text"" style=""display:none"" name=""artigosky"" ng-model=""formData.artigosky"" ng-if=""formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Tecnico' && formData.terceiroCombo === 'Codigo_1_2_5_7_8_11_ou_25'"" ng-init=""formData.artigosky = 'codigo1'"">");
            str.AppendLine(@"<input type=""text"" style=""display:none"" name=""artigosky"" ng-model=""formData.artigosky"" ng-if=""formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Tecnico' && formData.terceiroCombo === 'Codigo_6'""  ng-init=""formData.artigosky = 'Aparece mensagem com o código 6'"">");
            str.AppendLine(@"<input type=""text"" style=""display:none"" name=""artigosky"" ng-model=""formData.artigosky"" ng-if=""formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Tecnico' && formData.terceiroCombo === 'Codigo_13'""  ng-init=""formData.artigosky = 'Aparece mensagem com o código 13'"">");
            str.AppendLine(@"<input type=""text"" style=""display:none"" name=""artigosky"" ng-model=""formData.artigosky"" ng-if=""formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Tecnico' && formData.terceiroCombo === 'Codigo_77'""  ng-init=""formData.artigosky = 'Aparece mensagem com o código 77'"">");
            str.AppendLine(@"<input type=""text"" style=""display:none"" name=""artigosky"" ng-model=""formData.artigosky"" ng-if=""formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Tecnico' && formData.terceiroCombo === 'Codigo_109'""  ng-init=""formData.artigosky = 'Na tela da TV aparece código 109.'"">");
            str.AppendLine(@"<input type=""text"" style=""display:none"" name=""artigosky"" ng-model=""formData.artigosky"" ng-if=""formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Tecnico' && formData.terceiroCombo === 'Codigo_771_775_ou_776'"" ng-init=""formData.artigosky = 'codigo775'"">");
            str.AppendLine(@"<input type=""text"" style=""display:none"" name=""artigosky"" ng-model=""formData.artigosky"" ng-if=""formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Pacotes_e_Programacao' && formData.terceiroCombo === 'Big_Brother_Brasil'"" ng-init=""formData.artigosky = 'Informações sobre Big Brother Brasil.'"">");
            str.AppendLine(@"<input type=""text"" style=""display:none"" name=""artigosky"" ng-model=""formData.artigosky"" ng-if=""formData.origem && formData.primeiroCombo === 'sky_play'"" ng-init=""formData.artigosky = 'skyplay'"">");
            str.AppendLine(@"<input type=""text"" style=""display:none"" name=""artigosky"" ng-model=""formData.artigosky"" ng-if=""formData.origem && formData.primeiroCombo === 'sky_pre_pago' && formData.segundoCombo === 'Codigo_109'"" ng-init=""formData.artigosky = 'Na tela da TV aparece código 109.'"">");
            str.AppendLine(@"<input type=""text"" style=""display:none"" name=""artigosky"" ng-model=""formData.artigosky"" ng-if=""formData.origem && formData.primeiroCombo === 'sky_banda_larga' && formData.segundoCombo === 'Tecnico'""  ng-init=""formData.artigosky = 'Saiba o que pode influenciar a velocidade da sua internet.'"">");
            str.AppendLine(@"<input type=""text"" style=""display:none"" name=""artigosky"" ng-model=""formData.artigosky"" ng-if=""formData.origem && formData.primeiroCombo === 'sky_banda_larga' && formData.segundoCombo === 'Cobranca'""  ng-init=""formData.artigosky = 'Entenda como funciona a cobrança da SKY Banda Larga.'"">");
            str.AppendLine(@"<input type=""text"" style=""display:none"" name=""artigosky"" ng-model=""formData.artigosky"" ng-if=""formData.origem && formData.primeiroCombo === 'sky_banda_larga' && formData.segundoCombo === 'Aquisicao'""  ng-init=""formData.artigosky = 'Aquisição da Banda Larga.'"">");
            str.AppendLine(@"<input type=""text"" style=""display:none"" name=""artigosky"" ng-model=""formData.artigosky"" ng-if=""formData.origem && formData.primeiroCombo === 'sky_banda_larga' && formData.segundoCombo === 'Planos'""  ng-init=""formData.artigosky = 'Aquisição da Banda Larga.'"">");
            str.AppendLine(@"<input type=""text"" style=""display:none"" name=""artigosky"" ng-model=""formData.artigosky"" ng-if=""formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Tecnico' && formData.terceiroCombo === 'Visita_Técnica'""  ng-init=""formData.artigosky = 'visitatecnica'"">");
            str.AppendLine(@"<input type=""text"" style=""display:none"" name=""artigosky"" ng-model=""formData.artigosky"" ng-if=""formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Financeiro' && formData.terceiroCombo === 'Contestacao_de_valores'"" ng-init=""formData.artigosky = 'valores'"">");
            str.AppendLine(@"<input type=""text"" style=""display:none"" name=""artigosky"" ng-model=""formData.artigosky"" ng-if=""formData.origem && formData.primeiroCombo === 'tv_por_assinatura' && formData.segundoCombo === 'Financeiro' && formData.terceiroCombo === 'Duvida_na_fatura'"" ng-init=""formData.artigosky = 'duvida'"">");
            #endregion

            str.AppendLine(@"<div ng-show=""!(formData.terceiroCombo === 'Confirmacao_de_pagamento')"">");

            #region [ Header ]
            str.AppendLine(@"<div class=""dt-bot-header-wrapper"">");
            str.AppendLine(@" <div class=""dt-bot-image-header""></div>");
            str.AppendLine(@"    <div class=""dt-bot-text-header"">");
            str.AppendLine(@"        Olá, sou a Assistente Digital da SKY.<br/><br/>Vamos começar? <br/>É só responder a pergunta abaixo:");
            str.AppendLine("    </div>");
            str.AppendLine("</div>");
            #endregion

            #region [ Department ]
            str.AppendLine(@"<div class=""block-gray"">Você deseja mudar seu pacote de canais?</div>");
            //if (test)
            //    str.AppendLine(@"<div class='test'><br/><br/><br/>departamento: <strong>{{ formData.departamento }}</strong><br/>origem: <strong>{{ formData.origem }}</strong><br/>primeiroCombo: <strong>{{ formData.primeiroCombo }}</strong><br/>segundoCombo: <strong>{{ formData.segundoCombo }}</strong><br/>terceiroCombo: <strong>{{ formData.terceiroCombo }}</strong><br/>tier: <strong>{{ formData.tier2 }}</strong><br/><br/><br/><br/></div>");
            str.AppendLine(@"<div class=""radio_buttons"">");
            str.AppendLine(@"   <input type=""radio"" name=""departamento"" ng-model=""formData.departamento"" required value=""Sim""/> Sim");
            str.AppendLine(@"   <input type=""radio"" name=""departamento"" ng-model=""formData.departamento"" required value=""Nao""/> Não");
            str.AppendLine("</div>");
            #endregion

            #region[ Fields ]
            str.AppendLine(@"<input type=""text"" name=""nome_usuario"" ng-model=""formData.nome_usuario"" style=""display:none"">");
            str.AppendLine(@"<input type=""text"" name=""customernr"" ng-model=""formData.customernr"" key style=""display:none"">");
            str.AppendLine(@"<input type=""text"" name=""email"" ng-model=""formData.email"" key style=""display:none"">");
            str.AppendLine(@"<input type=""text"" name=""assunto"" ng-model=""formData.assunto"" style=""display:none"">");
            #endregion

            // Department true
            str.AppendLine(@"<div ng-if=""formData.departamento == 'Nao'"" class=""no-padding-div"">");

            #region [ Primeiro Combo ]
            str.AppendLine(@"<select ng-model=""formData.primeiroCombo"" name=""campo1"" ng-change=""formData.segundoCombo = null; formData.terceiroCombo = null;"" required subject>");
            str.AppendLine(@"<option value="""" selected disabled>ESCOLHA UM SERVIÇO ABAIXO</option>");
            foreach(var levelOne in list)
            {
                str.AppendLine(string.Format(@"<option value=""{0}"">{1}</option>", Utils.Slugfy(levelOne.Key), levelOne.Key));
            }
            str.AppendLine(@"   </select>");
            #endregion

            #region [ Segundo Combo ]
            foreach (var levelOne in list)
            {
                str.AppendLine(string.Format(@"<select category-updater ng-change=""formData.terceiroCombo = undefined;"" ng-if=""formData.primeiroCombo === '{0}'"" ng-model=""formData.segundoCombo"" name=""campo2"" required subject>", Utils.Slugfy(levelOne.Key)));
                str.AppendLine(string.Format(@"<option value="""" ng-if=""formData.primeiroCombo === '{0}'"" selected disabled>ESCOLHA UM ASSUNTO ABAIXO</option>", Utils.Slugfy(levelOne.Key)));

                Dictionary<string, string> categories = (Dictionary<string, string>)configuration.GetSection(string.Format("Categories:{0}", Utils.Slugfy(levelOne.Key))).Get(typeof(Dictionary<string, string>));

                foreach (var leveoTwo in ((Selector)levelOne.Value).Options)
                {
                    var category = string.Empty;
                    var hasCategory = categories.TryGetValue(Utils.Slugfy(leveoTwo.Key, false, false), out category);
                    if(hasCategory)
                        str.AppendLine(string.Format(@"<option value=""{0}"" category=""{1}"" ng-if=""formData.primeiroCombo == '{2}'"">{3}</option>", Utils.Slugfy(leveoTwo.Key, false, false), category, Utils.Slugfy(levelOne.Key), leveoTwo.Key));
                    else
                        str.AppendLine(string.Format(@"<option value=""{0}""  ng-if=""formData.primeiroCombo == '{1}'"">{2}</option>", Utils.Slugfy(leveoTwo.Key, false, false), Utils.Slugfy(levelOne.Key), leveoTwo.Key));
                }
                str.AppendLine(@"</select>");
            }
            #endregion

            #region [ Terceiro Combo ]
            foreach (var levelOne in list)
            {
                foreach (var levelTwo in ((Selector)levelOne.Value).Options)
                {
                    var aux = string.Empty;
                    var insert = false;
                    aux = string.Concat(aux, string.Format(@"<select ng-if=""formData.primeiroCombo === '{0}' && formData.segundoCombo === '{1}'"" ng-model=""formData.terceiroCombo"" name=""campo3"" required subject>", Utils.Slugfy(levelOne.Key), Utils.Slugfy(levelTwo.Key, false, false)));
                    aux = string.Concat(aux, string.Format(@"<option value="""" ng-if=""formData.primeiroCombo === '{0}' && formData.segundoCombo === '{1}'"" selected disabled>ESCOLHA UM ASSUNTO ABAIXO</option>", Utils.Slugfy(levelOne.Key), Utils.Slugfy(levelTwo.Key, false, false)));
                    foreach (var levelThree in ((Selector)levelTwo.Value).Options)
                    {
                        insert = levelThree.Value is Selector;
                        aux = string.Concat(aux, string.Format(@"<option value=""{0}"" ng-if=""formData.primeiroCombo === '{1}' && formData.segundoCombo == '{2}'"">{3}</option>", Utils.Slugfy(levelThree.Key, false, false), Utils.Slugfy(levelOne.Key), Utils.Slugfy(levelTwo.Key, false, false), levelThree.Key));
                    }
                    aux = string.Concat(aux, "</select>");
                    if (insert)
                        str.Append(aux);
                }
            }
            #endregion

            #region [ IDDs e Redirects ]
            foreach (var levelOne in list)
            {
                foreach (var levelTwo in ((Selector)levelOne.Value).Options)
                {
                    foreach(var levelThree in ((Selector)levelTwo.Value).Options)
                    {
                        // Tem terceira opção
                        if (levelThree.Value is Selector)
                        {
                            foreach (var option in ((Selector)levelThree.Value).Options)
                            {
                                var element = (SkyElement)option.Value;
                                str.AppendLine(string.Format(@"<input type=""hidden"" name=""idd"" ng-model=""formData.idd"" ng-if=""formData.origem==='{0}' && formData.primeiroCombo === '{1}' && formData.segundoCombo === '{2}' && formData.terceiroCombo === '{3}' && formData.{4} === 'true'"" value=""{5}""/>", 
                                                element.Origin, 
                                                Utils.Slugfy(levelOne.Key), 
                                                Utils.Slugfy(levelTwo.Key, false, false),
                                                Utils.Slugfy(levelThree.Key, false, false),
                                                GetTier(element.Tier),
                                                element.Idd
                                ));
                                str.AppendLine(GenerateSubmit(element, levelOne.Key, levelTwo.Key, levelThree.Key));
                            }
                        }
                        else
                        {
                            var element = (SkyElement)levelThree.Value;
                            str.AppendLine(string.Format(@"<input type=""hidden"" name=""idd"" ng-model=""formData.idd"" ng-if="" formData.origem==='{0}' && formData.primeiroCombo === '{1}' && formData.segundoCombo === '{2}' && formData.{3} === 'true'"" value=""{4}""/>",
                                            element.Origin,
                                            Utils.Slugfy(levelOne.Key),
                                            Utils.Slugfy(levelTwo.Key, false, false),
                                            GetTier(element.Tier),
                                            element.Idd
                            ));
                            str.AppendLine(GenerateSubmit(element, levelOne.Key, levelTwo.Key));
                        }
                    }
                }
            }
            #endregion

            str.AppendLine("</div>");

            #region End Department
            str.AppendLine("</div> <!-- End Dep -->");
            #endregion

            #region Department Sim
            str.AppendLine(@"<a ng-href="""" onclick=""window.initfake.openWindow()"" class=""btn-init-submit"" ng-if=""formData.departamento === 'Sim' && formData.origem!=='APP'"" ng-disabled=""form.$invalid"" target=""_blank"">Enviar</a>");
            str.AppendLine(@"<a ng-href="""" onclick=""window.initfake.openChat('D9E400704FDE601376B0')"" class=""btn-init-submit"" ng-if=""formData.departamento === 'Sim' && formData.origem==='APP'"" ng-disabled=""form.$invalid"" target=""_blank"">Enviar</a>");
            #endregion

            #region [ Confirmação de Pagamento ]
            str.AppendLine(@"<div ng-show=""formData.terceiroCombo === 'Confirmacao_de_pagamento'""><div ng-init=""startSearchingWithoutHiding('1d125184-0ca0-4e5b-b471-8ada4974180b')"" ng-if=""formData.terceiroCombo === 'Confirmacao_de_pagamento'""></div><div ng-show=""startSearchingSuccess"" class=""info""><h3>Atenção!</h3><p class=""grey-separator"">Prazo para a baixa de pagamento:<strong style=""display: block;""> Até 3 dias úteis</strong></p><div class=""small-white-boxy""><p><strong>Se o prazo não passou.</strong></p><p>Aguarde que a baixa do pagamento ocorrerá automaticamente.</p></div><div class=""small-white-boxy""><p><strong>Se estiver sem sinal.</strong></p><p>O sinal será reconectado automaticamente em até 48 horas, após a baixa do pagamento.</p></div><div class=""small-white-boxy""><p><strong>Se o prazo já passou.</strong></p><p>Encaminhe o comprovante de pagamento nos formatos JPG, BMP ou PDF.</p><button type=""button"" class=""sendmail"" onclick=""window.initfake.openMail()"">ENVIAR COMPROVANTE</button></div><div class=""right-small""><p>Se o prazo já passou e você já encaminhou o comprovante de pagamento,<button ng-if=""formData.master !== 'true' && formData.tier1 !== 'true'"" type=""button"" class=""no-style""  ng-click=""failRetention()"" onclick=""window.initfake.openChat('579')"">clique aqui</button><button ng-if=""formData.tier1  === 'true'"" type=""button"" class=""no-style""  ng-click=""failRetention()"" onclick=""window.initfake.openChat('B8E500579E7F001433BF')"">clique aqui</button><button ng-if=""formData.master === 'true'"" type=""button"" class=""no-style""  ng-click=""failRetention()"" onclick=""window.initfake.openChat('82BC00579DCEA015337D')"">clique aqui</button></p></div></div></div>");
            #endregion

            // Banner App
            str.AppendLine(@"<div class=""banner"">");
            str.AppendLine(@"	<img src=""https://files.directtalk.com.br/1.0/api/file/public/2e6bd8e4-0693-4cb5-8cfe-4773e1a8af86/content-inline"" width=""100%"">");
            str.AppendLine(@"	<a class=""ios"" href=""https://itunes.apple.com/br/app/minha-sky/id1154049541?mt=8"" target=""_blank"">a</a>");
            str.AppendLine(@"	<a class=""goo"" href=""https://play.google.com/store/apps/details?id=br.com.sky.selfcare&hl=pt_BR"" target=""_blank"">b</a>");
            str.AppendLine("</div>");

            // Styles
            str.AppendLine("<style>");
            str.AppendLine(".banner{margin:0!important;padding:0!important;position:relative}.banner .goo,.banner .ios{position:absolute;right:0;z-index:1;width:20%;height:50%;font-size:0}.banner .ios{top:0}.banner .goo{top:50%}.bot-initfake form{background:#F1F2F1;padding:0;font-size:13px;color:#573449;width:100%;height:100%;text-align:center;line-height:1.2em}.dt-bot-image-header,.dt-bot-text-header{display:inline-block!important;height:120px}.bot-initfake h3{font-size:15px;color:#ec1818;font-weight:700}.dt-bot-header-wrapper{width:100%}.dt-bot-text-header{width:50%!important;vertical-align:top;font-size:14px;font-weight:700;color:grey;text-align:initial}.dt-bot-image-header{width:48%!important;background-image:url(https://files.directtalk.com.br/1.0/api/file/public/3ee9154e-b4f7-4eba-a8bf-df2444543a62/content-inline);background-position:center;background-repeat:no-repeat}.small-white-boxy{padding:5px}.bot-initfake legend{margin:0 auto 10px;color:red;font-weight:700;padding:15px 0}.bot-initfake form div{display:block;width:100%;margin:0 auto;padding:5px 0}.bot-initfake form .radio_buttons{margin-top:10px;width:120px}.bot-initfake label{vertical-align:middle;display:inline-block;text-align:right}.btn-init-submit,.center{text-align:center}.bot-initfake form select{width:95%;margin:7px auto 0;border-radius:3px;border:1px solid #CCC;outline-color:#CCC;-webkit-appearance:none;-moz-appearance:none;appearance:none;background:#fff;padding:2px 7px}.btn-init-submit{border:0;background:#e0180c;color:#fff;font-size:12px;border-radius:2px;padding:4px 13px;cursor:pointer;display:inline-block;margin:0 auto;width:100px;text-transform:uppercase;text-decoration:none}.no-padding-div,.no-style{padding:0!important}.no-style{border:0!important;color:#1b47f2!important;text-transform:lowercase!important;font-size:12px!important;margin:0!important;background:rgba(0,0,0,0)!important;width:auto!important;text-decoration:underline!important;cursor:pointer}.block-gray{background:#d0d0d0;font-weight:700;color:#333;padding:5px 23px!important}.bot-initfake button[type=submit]:hover,.btn-init-submit:hover{background:#A90600}.bot-initfake p{margin:12px;color:#000}.bot-initfake small{padding:5px;font-size:8px;width:50%;margin:0 auto}.pre-text{color:#000!important;margin-bottom:24px!important}input.ng-invalid{border:1px solid red!important}input.ng-valid{border:1px solid green!important}.bold{font-weight:700}.small-white-boxy{background:#fff;font-size:12px;text-align:left;overflow:hidden;margin:5px 0!important;border-radius:5px}.small-white-boxy p{margin:2px 10px!important;color:#000}.info{padding:0 20px!important}.right-small{text-align:right}.right-small p{font-size:12px;margin:0!important}button.sendmail{border:0;color:#A90600;text-decoration:underline;background:#FFF;cursor:pointer;font-weight:700;padding:0 10px;display:block;font-size:12px}.grey-separator{background:silver;font-size:15px;margin:6px 0!important;padding:4px!important}");
            str.AppendLine("</style>");

            // Scripts
            str.AppendLine("<script type='text/javascript-lazy'>");
            str.AppendLine("window.initfake={},window.initfake.setDefault=function(e,n){\"\"!==e.value&&void 0!==e.value||(e.value=n,angular.element(e).triggerHandler(\"input\"))},window.initfake.validateOrigin=function(e){\"undefined\"!==e.target.value&&\"\"!==e.target.value||(e.target.value=\"Site SKY\")},window.initfake.validateOrigin=function(){var e=angular.element(document.querySelector(\"input[name = origem]\"));e.scope().$watch(function(){return e.scope().formData.origem},function(n,t){\"undefined\"!==n&&\"\"!==n&&void 0!==n||(e.scope().formData.origem=\"Site SKY\")})},window.initfake.setTiers=function(){var e=document.querySelector(\"input[name = tier2]\");angular.element(e).scope().formData.tier2=\"true\",document.querySelectorAll(\".hi - sky - tiers\").forEach(function(e){var n=-1!==document.location.hash.indexOf(e.name),t=angular.element(e).scope().formData;if(n){var a=document.location.hash.split(e.name+\" = \")[1].split(\" & \")[0];t[e.name]=a}else t[e.name]&&\"\"!==t[e.name].trim()||(t[e.name]=\"false\")})},window.initfake.reset=function(){\"parentIFrame\"in window&&parentIFrame.sendMessage({action:\"close\"})},window.initfake.openMail=function(){var e=document.querySelector(\"input[name = nome_usuario]\").value,n=\"https://contactmail.directtalk.com.br/Clientes/Sky/index.html?email=\"+document.querySelector(\"input[name=email]\").value+\"&nome_usuario=\"+e+\"&customernr=\"+document.querySelector(\"input[name=customernr]\").value;window.open(n,\"_blank\",\"toolbar=no,location=no,status=no,menubar=no,scrollbars=no,width=940,height=555,top=10,left=10\"),window.initfake.reset()},window.initfake.openWindow=function(){var e,n,t;e=document.querySelector(\"input[name=nome_usuario]\").value,n=document.querySelector(\"input[name=email]\").value,t=document.querySelector(\"input[name=customernr]\").value;var a=\"http://www2.directtalk.com.br/chat31/?idd=D9E400704FDE601376B0&origem=\"+document.querySelector(\"input[name=origem]\").value+\"&email=\"+n+\"&customernr=\"+t+\"&nome_usuario=\"+e+'&departamento=Sim&assunto=Sim\"';window.open(a,\"_blank\",\"toolbar=no,location=no,status=no,menubar=no,scrollbars=no,width=480,height=520\"),window.initfake.reset()},window.initfake.openChat=function(e){var n,t,a,o;n=document.querySelector(\"input[name=nome_usuario]\").value,t=document.querySelector(\"input[name=email]\").value,a=document.querySelector(\"input[name=customernr]\").value,o=document.querySelector(\"input[name=assunto]\").value;var i=\"https://www2.directtalk.com.br/chat/?idd=\"+e+\"&origem=\"+document.querySelector(\"input[name=origem]\").value+\"&email=\"+t+\"&customernr=\"+a+\"&nome_usuario=\"+n+\"&departamento=Nao&assunto=\"+o;window.open(i,\"_blank\",\"toolbar=no,location=no,status=no,menubar=no,scrollbars=no,width=480,height=520\"),window.initfake.reset()},window.initfake.changeElement=function(){var e,n,t,a;e=document.querySelector(\"input[name=assunto]\"),n=document.querySelector(\"select[name=campo1]\"),t=document.querySelector(\"select[name=campo2]\"),a=document.querySelector(\"select[name=campo3]\");var o=n.value;t&&(o+=\"+-+\"+t.value),a&&(o+=\"+-+\"+a.value),e.value=o,angular.element(e).triggerHandler(\"input\")},window.initfake.initialize=function(){window.initfake.setDefault(document.querySelector(\"input[name=origem]\"),\"Site SKY\"),window.initfake.validateOrigin(),window.initfake.setTiers(),window.localStorage.clear()},window.initfake.initialize();");
            str.AppendLine("</script>");
            str.AppendLine("</form>");

            if (test)
                str.AppendLine(@"</div></div></body></html>");
            return str.ToString();
        }

        /// <summary>
        /// Tudo igual, mas se no futuro mudar, case ta aí
        /// </summary>
        /// <param name="tier"></param>
        /// <returns></returns>
        private static string GetTier(string tier)
        {
            switch(Utils.RemoveDiacritics(tier).ToLower())
            {
                case "tier1":
                    return "tier1";
                case "tier2":
                    return "tier2";
                case "exclusivo":
                    return "exclusivo";
                case "prioritario":
                    return "prioritario";
                case "master":
                    return "master";
                default:
                    return "";
            }
        }

        private static string GenerateSubmit(SkyElement element, string levelOne, string levelTwo, string levelThree = null)
        {
            string result = string.Empty;
            if(element.Redirect.ToLower().IndexOf("dtchat") != -1)
            {
                result = string.Format(@"<a ng-href="""" onclick=""window.initfake.openChat('{0}')"" class=""btn-init-submit"" ng-if=""formData.departamento === 'Nao' && formData.origem==='{1}' && formData.primeiroCombo === '{2}' && formData.segundoCombo === '{3}' {4} && formData.{5} === 'true'"" ng-disabled=""form.$invalid"" target=""_blank"">Enviar</a>"
                                , element.Idd
                                , Utils.Slugfy(element.Origin)
                                , Utils.Slugfy(levelOne)
                                , Utils.Slugfy(levelTwo, false, false)
                                , !string.IsNullOrEmpty(levelThree) ? string.Format("&& formData.terceiroCombo === '{0}'", Utils.Slugfy(levelThree, false, false)) : string.Empty
                                , GetTier(element.Tier));
            }
            else if(element.Redirect.ToLower().IndexOf("dtbot") != -1)
            {
                result = string.Format(@"<button type=""submit"" ng-if=""formData.departamento === 'Nao' && formData.origem==='{1}' && formData.primeiroCombo === '{2}' && formData.segundoCombo === '{3}' {4} && formData.{5} === 'true'"" ng-disabled=""form.$invalid"" class=""btn-init-submit"">Enviar</button>"
                                , element.Idd
                                , Utils.Slugfy(element.Origin)
                                , Utils.Slugfy(levelOne)
                                , Utils.Slugfy(levelTwo, false, false)
                                , !string.IsNullOrEmpty(levelThree) ? string.Format("&& formData.terceiroCombo === '{0}'", Utils.Slugfy(levelThree, false, false)) : string.Empty
                                , GetTier(element.Tier));
            }
            return result;
        }
    }
}
